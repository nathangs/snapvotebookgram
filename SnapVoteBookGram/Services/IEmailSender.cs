﻿using System.Threading.Tasks;

namespace SnapVoteBookGram.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
