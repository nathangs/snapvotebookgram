﻿using System.Threading.Tasks;

namespace SnapVoteBookGram.Services
{
    public interface ISmsSender
    {
        Task SendSmsAsync(string number, string message);
    }
}
