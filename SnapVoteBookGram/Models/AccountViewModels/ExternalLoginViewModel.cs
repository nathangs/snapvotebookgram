using System.ComponentModel.DataAnnotations;

namespace SnapVoteBookGram.Models.AccountViewModels
{
    public class ExternalLoginViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
