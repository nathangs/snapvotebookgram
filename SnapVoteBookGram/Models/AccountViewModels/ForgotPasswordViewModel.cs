﻿using System.ComponentModel.DataAnnotations;

namespace SnapVoteBookGram.Models.AccountViewModels
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
