﻿namespace SnapVoteBookGram.Models
{
    public class Vote
    {
        public int Id { get; set; }
        public int ElectionId { get; set; }
        public int BallotId { get; set; }
        public int BallotOptionId { get; set; }
        public BallotOption BallotOption { get; set; }
        public int BallotOptionRank { get; set; }
        public string ApplicationUserId { get; set; }
    }
}
