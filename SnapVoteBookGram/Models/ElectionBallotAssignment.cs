﻿namespace SnapVoteBookGram.Models
{
    public class ElectionBallotAssignment
    {
        public int Id { get; set; }
        public int ElectionId { get; set; }
        public int BallotId { get; set; }
        public Election Election { get; set; }
        public Ballot Ballot { get; set; }
    }
}
