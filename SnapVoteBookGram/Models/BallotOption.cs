﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SnapVoteBookGram.Models
{
    public class BallotOption
    {
        public int Id { get; set; }
        public string Description { get; set; }
        private ICollection<BallotOptionAssignment> _ballotOptionAssignment;
        [Display(Name = "Ballot Options")]
        public ICollection<BallotOptionAssignment> BallotOptionAssignments
        {
            get
            {
                return _ballotOptionAssignment ?? (_ballotOptionAssignment = new List<BallotOptionAssignment>());
            }
            set
            {
                _ballotOptionAssignment = value;
            }
        }
    }
}
