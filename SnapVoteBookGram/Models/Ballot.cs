﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SnapVoteBookGram.Models
{

    public enum BallotType
    {
        Ranked, Unexpired, Dual
    }

    public class Ballot
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public string Description { get; set; }
        [Display(Name = "Ballot Type")]
        public BallotType? BallotType { get; set; }

        private ICollection<BallotOptionAssignment> _ballotOptionAssignment;
        [Display(Name = "Ballot Options")]
        public ICollection<BallotOptionAssignment> BallotOptionAssignments
        {
            get
            {
                return _ballotOptionAssignment ?? (_ballotOptionAssignment = new List<BallotOptionAssignment>());
            }
            set
            {
                _ballotOptionAssignment = value;
            }
        }

        private ICollection<ElectionBallotAssignment> _electionBallotAssignment;
        public ICollection<ElectionBallotAssignment> ElectionBallotAssignments
        {
            get
            {
                return _electionBallotAssignment ?? (_electionBallotAssignment = new List<ElectionBallotAssignment>());
            }
            set
            {
                _electionBallotAssignment = value;
            }
        }
    }
}
