﻿using System.Collections.Generic;

namespace SnapVoteBookGram.Models.ElectionViewModels
{
    public class ElectionsIndexData
    {
        public IEnumerable<Election> Elections { get; set; }
        public IEnumerable<Ballot> Ballots { get; set; }
    }
}
