﻿namespace SnapVoteBookGram.Models.ElectionViewModels
{
    public class AssignedBallotData
    {
        public int BallotId { get; set; }
        public string Title { get; set; }
        public bool Assigned { get; set; }
    }
}
