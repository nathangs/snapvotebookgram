﻿namespace SnapVoteBookGram.Models
{
    public class BallotOptionAssignment
    {
        public int Id { get; set; }
        public int BallotId { get; set; }
        public int BallotOptionId { get; set; }
        public Ballot Ballot { get; set; }
        public BallotOption BallotOption { get; set; }
    }
}
