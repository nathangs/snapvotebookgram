﻿namespace SnapVoteBookGram.Models.BallotViewModels
{
    public class AssignedBallotOptionData
    {
        public int BallotOptionId { get; set; }
        public string Description { get; set; }
        public bool Assigned { get; set; }
    }
}
