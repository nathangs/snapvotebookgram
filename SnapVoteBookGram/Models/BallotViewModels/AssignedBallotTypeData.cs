﻿namespace SnapVoteBookGram.Models.BallotViewModels
{
    public class AssignedBallotTypeData
    {
        public string Title { get; set; }
        public bool Assigned { get; set; }
    }
}
