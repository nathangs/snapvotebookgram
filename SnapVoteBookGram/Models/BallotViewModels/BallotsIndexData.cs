﻿using System.Collections.Generic;

namespace SnapVoteBookGram.Models.BallotViewModels
{
    public class BallotsIndexData
    {
        public IEnumerable<Ballot> Ballots { get; set; }
    }
}
