﻿using System;
using System.Collections.Generic;

namespace SnapVoteBookGram.Models
{
    public class Election
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime Begin { get; set; }
        public DateTime End { get; set; }

        private ICollection<ElectionBallotAssignment> _electionBallotAssignment;
        public ICollection<ElectionBallotAssignment> ElectionBallotAssignments
        {
            get
            {
                return _electionBallotAssignment ?? (_electionBallotAssignment = new List<ElectionBallotAssignment>());
            }
            set
            {
                _electionBallotAssignment = value;
            }
        }
    }
}
