﻿using System.Collections.Generic;

namespace SnapVoteBookGram.Models.VoteViewModels
{
    public class ElectionBallotsIndexData
    {
        public Election Election { get; set; }
        public IEnumerable<Ballot> Ballots { get; set; }
    }
}
