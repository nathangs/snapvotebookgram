﻿using System.Collections.Generic;

namespace SnapVoteBookGram.Models.VoteViewModels
{
    public class VoteRankResult
    {
        public int Rank { get; set; }
        public List<VoteResult> Votes { get; set; }
    }
}
