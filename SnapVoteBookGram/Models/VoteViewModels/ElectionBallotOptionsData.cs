﻿using System.Collections.Generic;

namespace SnapVoteBookGram.Models.VoteViewModels
{
    public class ElectionBallotOptionsData
    {
        public Election Election { get; set; }
        public Ballot Ballot { get; set; }

        private ICollection<BallotOption> _ballotOptions;
        public ICollection<BallotOption> BallotOptions
        {
            get
            {
                return _ballotOptions ?? (_ballotOptions = new List<BallotOption>());
            }
            set
            {
                _ballotOptions = value;
            }
        }
    }
}
