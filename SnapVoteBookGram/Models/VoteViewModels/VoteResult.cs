﻿namespace SnapVoteBookGram.Models.VoteViewModels
{
    public class VoteResult
    {
        public int Id { get; set; }
        public int Count { get; set; }
        public int Rank { get; set; }
        public string Description { get; set; }
    }
}
