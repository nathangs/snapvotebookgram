﻿using System.Collections.Generic;

namespace SnapVoteBookGram.Models.VoteViewModels
{
    public class ResultsData
    {
        public IEnumerable<Election> Elections { get; set; }
        public Election selectedElection { get; set; }
        public IEnumerable<Ballot> Ballots { get; set; }
        public Ballot selectedBallot { get; set; }
        public IEnumerable<BallotOption> BallotOptions { get; set; }
        public IEnumerable<VoteResult> Votes { get; set; }
        public List<VoteRankResult> RankedVotes { get; set; }
    }
}
