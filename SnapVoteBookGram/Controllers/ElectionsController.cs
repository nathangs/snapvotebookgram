﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SnapVoteBookGram.Data;
using SnapVoteBookGram.Models;
using SnapVoteBookGram.Models.ElectionViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SnapVoteBookGram.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class ElectionsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ElectionsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Elections
        public async Task<IActionResult> Index()
        {
            var viewModel = new ElectionsIndexData();
            viewModel.Elections = await _context.Elections
                  .Include(i => i.ElectionBallotAssignments)
                    .ThenInclude(i => i.Ballot)
                  .AsNoTracking()
                  .ToListAsync();

            return View(viewModel);
        }

        // GET: Elections/Create
        public IActionResult Create()
        {
            var election = new Election();
            PopulateAssignedBallotData(election);
            return View();
        }

        // POST: Elections/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Title,Begin,End")] Election election, string[] selectedBallots)
        {
            if (selectedBallots != null)
            {
                election.ElectionBallotAssignments = new List<ElectionBallotAssignment>();
                foreach (var ballot in selectedBallots)
                {
                    var ballotToAdd = new ElectionBallotAssignment { ElectionId = election.Id, BallotId = int.Parse(ballot) };
                    election.ElectionBallotAssignments.Add(ballotToAdd);
                }
            }
            if (ModelState.IsValid)
            {
                _context.Add(election);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            PopulateAssignedBallotData(election);
            return View(election);
        }

        // GET: Elections/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var election = await _context.Elections
                .Include(i => i.ElectionBallotAssignments).ThenInclude(i => i.Ballot)
                .AsNoTracking()
                .SingleOrDefaultAsync(m => m.Id == id);
            if (election == null)
            {
                return NotFound();
            }

            PopulateAssignedBallotData(election);
            return View(election);
        }

        // POST: Elections/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int? id, string[] selectedBallots)
        {
            if (id == null)
            {
                return NotFound();
            }

            var electionToUpdate = await _context.Elections
                .Include(i => i.ElectionBallotAssignments)
                    .ThenInclude(i => i.Ballot)
                .SingleOrDefaultAsync(m => m.Id == id);

            if (await TryUpdateModelAsync<Election>(electionToUpdate, "", i => i.Title, i => i.Begin, i => i.End))
            {
                UpdateBallots(selectedBallots, electionToUpdate);
                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (Exception)
                {
                    ModelState.AddModelError("", "There was a problem saving your changes. Please try again.");
                }
                return RedirectToAction(nameof(Index));
            }
            UpdateBallots(selectedBallots, electionToUpdate);
            PopulateAssignedBallotData(electionToUpdate);
            return View(electionToUpdate);
        }

        // GET: Elections/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var election = await _context.Elections
                .SingleOrDefaultAsync(m => m.Id == id);
            if (election == null)
            {
                return NotFound();
            }

            return View(election);
        }

        // POST: Elections/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var election = await _context.Elections.SingleOrDefaultAsync(m => m.Id == id);
            _context.Elections.Remove(election);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ElectionExists(int id)
        {
            return _context.Elections.Any(e => e.Id == id);
        }

        private void PopulateAssignedBallotData(Election election)
        {
            var allBallots = _context.Ballots;
            var currentBallots = new HashSet<int>(election.ElectionBallotAssignments.Select(c => c.BallotId));
            var viewModel = new List<AssignedBallotData>();
            foreach (var ballot in allBallots)
            {
                viewModel.Add(new AssignedBallotData
                {
                    BallotId = ballot.Id,
                    Title = ballot.Title,
                    Assigned = currentBallots.Contains(ballot.Id)
                });
            }
            ViewData["Ballots"] = viewModel.OrderBy(i => i.Title).ToList();
        }

        private void UpdateBallots(string[] selectedBallots, Election electionToUpdate)
        {
            if (selectedBallots == null)
            {
                electionToUpdate.ElectionBallotAssignments = new List<ElectionBallotAssignment>();
                return;
            }

            var selectedBallotsHashSet = new HashSet<string>(selectedBallots);
            var currentBallots = new HashSet<int>(electionToUpdate.ElectionBallotAssignments.Select(c => c.Ballot.Id));

            foreach (var ballot in _context.Ballots)
            {
                if (selectedBallotsHashSet.Contains(ballot.Id.ToString()))
                {
                    if (!currentBallots.Contains(ballot.Id))
                    {
                        electionToUpdate.ElectionBallotAssignments.Add(new ElectionBallotAssignment { ElectionId = electionToUpdate.Id, BallotId = ballot.Id });
                    }
                }
                else
                {
                    if (currentBallots.Contains(ballot.Id))
                    {
                        ElectionBallotAssignment ballotToRemove = electionToUpdate.ElectionBallotAssignments.SingleOrDefault(i => i.BallotId == ballot.Id);
                        _context.Remove(ballotToRemove);
                    }
                }
            }
        }

    }
}
