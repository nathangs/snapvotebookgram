﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SnapVoteBookGram.Data;
using SnapVoteBookGram.Models;
using SnapVoteBookGram.Models.BallotViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SnapVoteBookGram.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class BallotsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public BallotsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Ballots
        public async Task<IActionResult> Index()
        {

            var viewModel = new BallotsIndexData();
            viewModel.Ballots = await _context.Ballots
                  .Include(i => i.BallotOptionAssignments)
                    .ThenInclude(i => i.BallotOption)
                  .AsNoTracking()
                  .ToListAsync();

            return View(viewModel);
        }

        // GET: Ballots/Create
        public IActionResult Create()
        {
            var ballot = new Ballot();
            PopulateAssignedBallotTypeData(ballot);
            PopulateAssignedBallotOptionData(ballot);
            return View();
        }

        // POST: Ballots/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Title,Subtitle,Description,BallotType")] Ballot ballot, string[] selectedBallotOptions)
        {
            if (selectedBallotOptions != null)
            {
                ballot.BallotOptionAssignments = new List<BallotOptionAssignment>();
                foreach (var ballotOption in selectedBallotOptions)
                {
                    var ballotOptionToAdd = new BallotOptionAssignment { BallotId = ballot.Id, BallotOptionId = int.Parse(ballotOption) };
                    ballot.BallotOptionAssignments.Add(ballotOptionToAdd);
                }
            }
            if (ModelState.IsValid)
            {
                _context.Add(ballot);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            PopulateAssignedBallotOptionData(ballot);
            return View(ballot);
        }

        // GET: Ballots/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ballot = await _context.Ballots
                .Include(i => i.BallotOptionAssignments).ThenInclude(i => i.BallotOption)
                .AsNoTracking()
                .SingleOrDefaultAsync(m => m.Id == id);
            if (ballot == null)
            {
                return NotFound();
            }

            PopulateAssignedBallotTypeData(ballot);
            PopulateAssignedBallotOptionData(ballot);
            return View(ballot);
        }

        // POST: Ballots/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int? id, string[] selectedBallotOptions)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ballotToUpdate = await _context.Ballots
                .Include(i => i.BallotOptionAssignments)
                    .ThenInclude(i => i.BallotOption)
                .SingleOrDefaultAsync(m => m.Id == id);

            if (await TryUpdateModelAsync<Ballot>(ballotToUpdate, "", i => i.Title, i => i.Subtitle, i => i.Description, i => i.BallotType))
            {
                UpdateBallotOptions(selectedBallotOptions, ballotToUpdate);
                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (Exception)
                {
                    ModelState.AddModelError("", "There was a problem saving your changes. Please try again.");
                }
                return RedirectToAction(nameof(Index));
            }
            UpdateBallotOptions(selectedBallotOptions, ballotToUpdate);
            PopulateAssignedBallotOptionData(ballotToUpdate);
            return View(ballotToUpdate);
        }

        // GET: Ballots/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ballot = await _context.Ballots
                .SingleOrDefaultAsync(m => m.Id == id);
            if (ballot == null)
            {
                return NotFound();
            }

            return View(ballot);
        }

        // POST: Ballots/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var ballot = await _context.Ballots.SingleOrDefaultAsync(m => m.Id == id);
            _context.Ballots.Remove(ballot);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BallotExists(int id)
        {
            return _context.Ballots.Any(e => e.Id == id);
        }

        private void PopulateAssignedBallotTypeData(Ballot ballot)
        {
            var allBallotTypes = new string[] {
                BallotType.Dual.ToString(),
                BallotType.Ranked.ToString(),
                BallotType.Unexpired.ToString()
            };
            var selectedBallotType = ballot.BallotType;
            var viewModel = new List<AssignedBallotTypeData>();
            foreach (var ballotType in allBallotTypes)
            {
                viewModel.Add(new AssignedBallotTypeData
                {
                    Title = ballotType,
                    Assigned = selectedBallotType.Equals(ballotType)
                });
            }
            ViewData["BallotTypes"] = viewModel;
        }

        /// <summary>
        /// Get assigned ballot options
        /// </summary>
        /// <param name="ballot"></param>
        private void PopulateAssignedBallotOptionData(Ballot ballot)
        {
            var allBallotOptions = _context.BallotOptions;
            var currentBallotOptions = new HashSet<int>(ballot.BallotOptionAssignments.Select(c => c.BallotOptionId));
            var viewModel = new List<AssignedBallotOptionData>();
            foreach (var ballotOption in allBallotOptions)
            {
                viewModel.Add(new AssignedBallotOptionData
                {
                    BallotOptionId = ballotOption.Id,
                    Description = ballotOption.Description,
                    Assigned = currentBallotOptions.Contains(ballotOption.Id)
                });
            }
            ViewData["BallotOptions"] = viewModel.OrderBy(i => i.Description).ToList();
        }

        /// <summary>
        /// Update ballot options
        /// </summary>
        /// <param name="selectedBallotOptions"></param>
        /// <param name="ballotToUpdate"></param>
        private void UpdateBallotOptions(string[] selectedBallotOptions, Ballot ballotToUpdate)
        {
            if (selectedBallotOptions == null)
            {
                ballotToUpdate.BallotOptionAssignments = new List<BallotOptionAssignment>();
                return;
            }

            var selectedBallotOptionsHashSet = new HashSet<string>(selectedBallotOptions);
            var currentBallotOptions = new HashSet<int>(ballotToUpdate.BallotOptionAssignments.Select(c => c.BallotOption.Id));

            foreach (var ballotOption in _context.BallotOptions)
            {
                if (selectedBallotOptionsHashSet.Contains(ballotOption.Id.ToString()))
                {
                    if (!currentBallotOptions.Contains(ballotOption.Id))
                    {
                        ballotToUpdate.BallotOptionAssignments.Add(new BallotOptionAssignment { BallotId = ballotToUpdate.Id, BallotOptionId = ballotOption.Id });
                    }
                }
                else
                {
                    if (currentBallotOptions.Contains(ballotOption.Id))
                    {
                        BallotOptionAssignment ballotOptionToRemove = ballotToUpdate.BallotOptionAssignments.SingleOrDefault(i => i.BallotOptionId == ballotOption.Id);
                        _context.Remove(ballotOptionToRemove);
                    }
                }
            }
        }

    }
}
