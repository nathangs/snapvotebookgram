﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SnapVoteBookGram.Data;
using SnapVoteBookGram.Models;
using System.Linq;
using System.Threading.Tasks;

namespace SnapVoteBookGram.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class BallotOptionsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public BallotOptionsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: BallotOptions
        public async Task<IActionResult> Index()
        {
            return View(await _context.BallotOptions.ToListAsync());
        }

        // GET: BallotOptions/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: BallotOptions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Description")] BallotOption ballotOption)
        {
            if (ModelState.IsValid)
            {
                _context.Add(ballotOption);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(ballotOption);
        }

        // GET: BallotOptions/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ballotOption = await _context.BallotOptions.SingleOrDefaultAsync(m => m.Id == id);
            if (ballotOption == null)
            {
                return NotFound();
            }
            return View(ballotOption);
        }

        // POST: BallotOptions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Description")] BallotOption ballotOption)
        {
            if (id != ballotOption.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(ballotOption);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BallotOptionExists(ballotOption.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(ballotOption);
        }

        // GET: BallotOptions/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ballotOption = await _context.BallotOptions
                .SingleOrDefaultAsync(m => m.Id == id);
            if (ballotOption == null)
            {
                return NotFound();
            }

            return View(ballotOption);
        }

        // POST: BallotOptions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var ballotOption = await _context.BallotOptions.SingleOrDefaultAsync(m => m.Id == id);
            _context.BallotOptions.Remove(ballotOption);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BallotOptionExists(int id)
        {
            return _context.BallotOptions.Any(e => e.Id == id);
        }
    }
}
