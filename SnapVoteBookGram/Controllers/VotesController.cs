﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SnapVoteBookGram.Data;
using SnapVoteBookGram.Models;
using SnapVoteBookGram.Models.ElectionViewModels;
using SnapVoteBookGram.Models.VoteViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SnapVoteBookGram.Controllers
{
    public class VotesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public VotesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Elections
        public async Task<IActionResult> Elections()
        {
            var viewModel = new ElectionsIndexData();
            viewModel.Elections = await _context.Elections
                .Where(i => i.End > DateTime.Now)
                  .AsNoTracking()
                  .ToListAsync();

            return View(viewModel);
        }

        // GET: ElectionBallots
        public async Task<IActionResult> ElectionBallots(int? id)
        {
            //get current userId
            ClaimsPrincipal currentUser = User;
            var userId = currentUser.FindFirst(ClaimTypes.NameIdentifier).Value;

            ViewData["ElectionId"] = id.Value;

            var viewModel = new ElectionBallotsIndexData();
            
            var data = await _context.Elections
                .Include(i => i.ElectionBallotAssignments)
                    .ThenInclude(b => b.Ballot)
                        .ThenInclude(o => o.BallotOptionAssignments)
                            .ThenInclude(p => p.BallotOption).AsNoTracking().ToListAsync();

            viewModel.Election = data
                .Where(i => i.Id == id.Value).Single();

            var BallotsUserHasVotedIn = await _context.Votes
                .Where(v => v.ApplicationUserId == userId)
                .Select(v => v.BallotId)
                .ToListAsync();

            //only display ballots for election that user hasn't already voted in
            viewModel.Ballots = viewModel.Election.ElectionBallotAssignments
                .Select(o => o.Ballot)
                .Where(i => !BallotsUserHasVotedIn.Contains(i.Id))
                .ToList();

            return View(viewModel);
        }

        // GET: ElectionBallotOptions
        public async Task<IActionResult> ElectionBallotOptions(int? electionId, int? ballotId)
        {
            ViewData["ElectionId"] = electionId.Value;
            ViewData["BallotId"] = ballotId.Value;

            var viewModel = new ElectionBallotOptionsData();

            var data = await _context.Elections
                .Include(i => i.ElectionBallotAssignments)
                    .ThenInclude(b => b.Ballot)
                        .ThenInclude(o => o.BallotOptionAssignments)
                            .ThenInclude(p => p.BallotOption).AsNoTracking().ToListAsync();

            viewModel.Election = data
                .Where(i => i.Id == electionId.Value).Single();

            viewModel.Ballot = viewModel.Election.ElectionBallotAssignments
                .Where(b => b.BallotId == ballotId.Value).Single().Ballot;

            viewModel.BallotOptions = viewModel.Ballot.BallotOptionAssignments
                .Select(o => o.BallotOption).ToList();

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Vote(int? electionId, int? ballotId, string[] selectedBallotOptions, string[] selectedBallotOptionValues)
        {
            //get current userId
            ClaimsPrincipal currentUser = User;
            var userId = currentUser.FindFirst(ClaimTypes.NameIdentifier).Value;

            //get current ballot
            Ballot selectedBallot = _context.Ballots
                    .Where(b => b.Id == ballotId.Value).Single();

            List<Vote> votesToSave = new List<Vote>();

            //generate vote for each ballot option
            for (int count = 0; count < selectedBallotOptions.Length; count++)
            {
                Vote vote = new Vote();
                
                //for ranked voting
                if (selectedBallot.BallotType == BallotType.Ranked)
                {
                    if (selectedBallotOptionValues != null && selectedBallotOptionValues.Length != 0 && selectedBallotOptionValues.Length >= count)
                    {
                        //only create vote if we have a valid rank value
                        int.TryParse(selectedBallotOptionValues[count], out int rank);
                        if (rank > 0)
                        {
                            vote = new Models.Vote()
                            {
                                ElectionId = electionId.Value,
                                BallotId = ballotId.Value,
                                BallotOptionId = int.Parse(selectedBallotOptions[count]),
                                ApplicationUserId = userId,
                                BallotOptionRank = rank
                            };
                            votesToSave.Add(vote);
                        }
                    }
                }
                //we don't have to worry about ranking for other ballot types
                else
                {
                    vote = new Models.Vote()
                    {
                        ElectionId = electionId.Value,
                        BallotId = ballotId.Value,
                        BallotOptionId = int.Parse(selectedBallotOptions[count]),
                        ApplicationUserId = userId
                    };
                    votesToSave.Add(vote);
                }
            }

            //save votes to database
            if (ModelState.IsValid && votesToSave.Count > 0)
            {
                _context.AddRange(votesToSave);
                await _context.SaveChangesAsync();
            }

            return RedirectToAction(nameof(Elections));
        }

        // GET: Votes
        [AllowAnonymous]
        public async Task<IActionResult> Index(int? electionId, int? ballotId)
        {
            var viewModel = new ResultsData();

            viewModel.Elections = await _context.Elections
                .Where(e => e.End < DateTime.Now)
                .Include(i => i.ElectionBallotAssignments)
                    .ThenInclude(b => b.Ballot)
                        .ThenInclude(o => o.BallotOptionAssignments)
                            .ThenInclude(p => p.BallotOption).AsNoTracking().ToListAsync();

            if (electionId != null)
            {
                ViewData["ElectionId"] = electionId.Value;
                viewModel.selectedElection = viewModel.Elections
                    .Where(e => e.Id == electionId.Value).Single();
                viewModel.Ballots = viewModel.selectedElection.ElectionBallotAssignments
                    .Select(o => o.Ballot).ToList();
            }

            if (ballotId != null)
            {
                ViewData["BallotId"] = ballotId.Value;
                viewModel.selectedBallot = viewModel.Ballots
                    .Where(b => b.Id == ballotId.Value).Single();

                //get all votes for current ballot
                var votes = await _context.Votes
                    .Where(e => e.ElectionId == electionId.Value)
                    .Where(b => b.BallotId == ballotId.Value)
                    .Include(o => o.BallotOption)
                    .ToListAsync();

                //sort votes rank for display
                if (viewModel.selectedBallot.BallotType == BallotType.Ranked)
                {

                    List<VoteRankResult> results = new List<VoteRankResult>();

                    var rankedVotes = votes
                        .GroupBy(g => g.BallotOptionRank).ToList();

                    foreach (var rank in rankedVotes)
                    {
                        VoteRankResult voteRankResult = new VoteRankResult()
                        {
                            Rank = rank.Key,
                            Votes = rank
                                .GroupBy(g => g.BallotOption)
                                .Select(group => new VoteResult
                                {
                                    Id = group.Key.Id,
                                    Description = group.Key.Description,
                                    Count = group.Count()
                                })
                                .OrderByDescending(x => x.Count).ToList()
                        };

                        results.Add(voteRankResult);
                    }

                    viewModel.RankedVotes = results;

                }
                //sort votes by count for display
                else
                {
                    viewModel.Votes = votes
                        .GroupBy(g => g.BallotOption)
                        .Select(group => new VoteResult
                        {
                            Id = group.Key.Id,
                            Description = group.Key.Description,
                            Count = group.Count()
                        })
                        .OrderByDescending(x => x.Count).ToList();
                }
            }

            return View(viewModel);
        }

        private bool VoteExists(int id)
        {
            return _context.Votes.Any(e => e.Id == id);
        }
    }
}
