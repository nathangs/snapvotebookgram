﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace SnapVoteBookGram.Data.Migrations
{
    public partial class UpdateIds : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BallotOption_Ballot_BallotID",
                table: "BallotOption");

            migrationBuilder.RenameColumn(
                name: "BallotID",
                table: "Vote",
                newName: "BallotId");

            migrationBuilder.RenameColumn(
                name: "ID",
                table: "Vote",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "BallotID",
                table: "BallotOption",
                newName: "BallotId");

            migrationBuilder.RenameColumn(
                name: "ID",
                table: "BallotOption",
                newName: "Id");

            migrationBuilder.RenameIndex(
                name: "IX_BallotOption_BallotID",
                table: "BallotOption",
                newName: "IX_BallotOption_BallotId");

            migrationBuilder.RenameColumn(
                name: "ID",
                table: "Ballot",
                newName: "Id");

            migrationBuilder.AddColumn<int>(
                name: "ApplicationUserId",
                table: "Vote",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "ApplicationUserId1",
                table: "Vote",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BallotOptionId",
                table: "Vote",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ElectionId",
                table: "Vote",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ElectionId",
                table: "Ballot",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Election",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Begin = table.Column<DateTime>(type: "datetime2", nullable: false),
                    End = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Election", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Vote_ApplicationUserId1",
                table: "Vote",
                column: "ApplicationUserId1");

            migrationBuilder.CreateIndex(
                name: "IX_Ballot_ElectionId",
                table: "Ballot",
                column: "ElectionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Ballot_Election_ElectionId",
                table: "Ballot",
                column: "ElectionId",
                principalTable: "Election",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BallotOption_Ballot_BallotId",
                table: "BallotOption",
                column: "BallotId",
                principalTable: "Ballot",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Vote_AspNetUsers_ApplicationUserId1",
                table: "Vote",
                column: "ApplicationUserId1",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Ballot_Election_ElectionId",
                table: "Ballot");

            migrationBuilder.DropForeignKey(
                name: "FK_BallotOption_Ballot_BallotId",
                table: "BallotOption");

            migrationBuilder.DropForeignKey(
                name: "FK_Vote_AspNetUsers_ApplicationUserId1",
                table: "Vote");

            migrationBuilder.DropTable(
                name: "Election");

            migrationBuilder.DropIndex(
                name: "IX_Vote_ApplicationUserId1",
                table: "Vote");

            migrationBuilder.DropIndex(
                name: "IX_Ballot_ElectionId",
                table: "Ballot");

            migrationBuilder.DropColumn(
                name: "ApplicationUserId",
                table: "Vote");

            migrationBuilder.DropColumn(
                name: "ApplicationUserId1",
                table: "Vote");

            migrationBuilder.DropColumn(
                name: "BallotOptionId",
                table: "Vote");

            migrationBuilder.DropColumn(
                name: "ElectionId",
                table: "Vote");

            migrationBuilder.DropColumn(
                name: "ElectionId",
                table: "Ballot");

            migrationBuilder.RenameColumn(
                name: "BallotId",
                table: "Vote",
                newName: "BallotID");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Vote",
                newName: "ID");

            migrationBuilder.RenameColumn(
                name: "BallotId",
                table: "BallotOption",
                newName: "BallotID");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "BallotOption",
                newName: "ID");

            migrationBuilder.RenameIndex(
                name: "IX_BallotOption_BallotId",
                table: "BallotOption",
                newName: "IX_BallotOption_BallotID");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Ballot",
                newName: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_BallotOption_Ballot_BallotID",
                table: "BallotOption",
                column: "BallotID",
                principalTable: "Ballot",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
