﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using SnapVoteBookGram.Models;
using System;
using System.Linq;

namespace SnapVoteBookGram.Data
{

    //adapted from https://stackoverflow.com/questions/34343599/how-to-seed-users-and-roles-with-code-first-migration-using-identity-asp-net-mvc

    public class DbAccountInitializer
    {
        private ApplicationDbContext _context;

        public DbAccountInitializer(ApplicationDbContext context)
        {
            _context = context;
        }

        public async void SeedUsers()
        {
            _context.Database.EnsureCreated();

            if (_context.Users.Any())
            {
                return; //db has already been seeded
            }

            //create admin role
            var roleStore = new RoleStore<IdentityRole>(_context);
            if (!_context.Roles.Any(r => r.Name == "Administrator"))
            {
                roleStore.CreateAsync(new IdentityRole { Name = "Administrator", NormalizedName = "Administrator" }).Wait();
            }

            //seed admin user
            var adminUser = new ApplicationUser
            {
                UserName = "mailboxngs@gmail.com",
                NormalizedUserName = "mailboxngs@gmail.com",
                Email = "mailboxngs@gmail.com",
                NormalizedEmail = "mailboxngs@gmail.com",
                EmailConfirmed = true,
                LockoutEnabled = false,
                SecurityStamp = Guid.NewGuid().ToString()
            };

            if (!_context.Users.Any(u => u.UserName == adminUser.UserName))
            {
                var password = new PasswordHasher<ApplicationUser>();
                var hashed = password.HashPassword(adminUser, "Password1234!");
                adminUser.PasswordHash = hashed;
                var userStore = new UserStore<ApplicationUser>(_context);
                userStore.CreateAsync(adminUser).Wait();
                userStore.AddToRoleAsync(adminUser, "Administrator").Wait();
            }

            //seed regular user
            var user = new ApplicationUser
            {
                UserName = "nathan@nathansmetana.com",
                NormalizedUserName = "nathan@nathansmetana.com",
                Email = "nathan@nathansmetana.com",
                NormalizedEmail = "nathan@nathansmetana.com",
                EmailConfirmed = true,
                SecurityStamp = Guid.NewGuid().ToString()
            };

            if (!_context.Users.Any(u => u.UserName == user.UserName))
            {
                var password = new PasswordHasher<ApplicationUser>();
                var hashed = password.HashPassword(adminUser, "Password1234!");
                user.PasswordHash = hashed;
                var userStore = new UserStore<ApplicationUser>(_context);
                userStore.CreateAsync(user).Wait();
            }

            var user2 = new ApplicationUser
            {
                UserName = "fake@fake.com",
                NormalizedUserName = "fake@fake.com",
                Email = "fake@fake.com",
                NormalizedEmail = "fake@fake.com",
                EmailConfirmed = true,
                SecurityStamp = Guid.NewGuid().ToString()
            };

            if (!_context.Users.Any(u => u.UserName == user2.UserName))
            {
                var password = new PasswordHasher<ApplicationUser>();
                var hashed = password.HashPassword(adminUser, "Password1234!");
                user2.PasswordHash = hashed;
                var userStore = new UserStore<ApplicationUser>(_context);
                userStore.CreateAsync(user2).Wait();
            }

            var user3 = new ApplicationUser
            {
                UserName = "fake2@fake2.com",
                NormalizedUserName = "fake2@fake2.com",
                Email = "fake2@fake2.com",
                NormalizedEmail = "fake2@fake2.com",
                EmailConfirmed = true,
                SecurityStamp = Guid.NewGuid().ToString()
            };

            if (!_context.Users.Any(u => u.UserName == user3.UserName))
            {
                var password = new PasswordHasher<ApplicationUser>();
                var hashed = password.HashPassword(adminUser, "Password1234!");
                user3.PasswordHash = hashed;
                var userStore = new UserStore<ApplicationUser>(_context);
                userStore.CreateAsync(user3).Wait();
            }

            _context.SaveChangesAsync().Wait();
        }
    }
}
