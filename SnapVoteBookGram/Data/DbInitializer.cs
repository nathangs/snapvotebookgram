﻿using SnapVoteBookGram.Models;
using System;
using System.Linq;

namespace SnapVoteBookGram.Data
{
    public static class DbInitializer
    {
        public static void Initialize(ApplicationDbContext context)
        {
            context.Database.EnsureCreated();

            if (context.Elections.Any())
            {
                return; //db already has data
            }

            var elections = new Election[]
            {
                new Election{
                    Title ="FEDERAL AND STATE",
                    Begin = DateTime.Now.AddDays(-1),
                    End = DateTime.Now.AddDays(30)
                },
                new Election{
                    Title ="COUNTY",
                    Begin = DateTime.Now.AddDays(-1),
                    End = DateTime.Now.AddDays(30)
                },
                new Election{
                    Title ="Federal Election (Completed with Results)",
                    Begin = DateTime.Now.AddDays(-30),
                    End = DateTime.Now.AddDays(-1)
                },
                new Election{
                    Title ="Future Election",
                    Begin = DateTime.Now.AddDays(365),
                    End = DateTime.Now.AddDays(395)
                }                
            };
            foreach (Election e in elections)
            {
                context.Elections.Add(e);
            }
            context.SaveChanges();

            var ballots = new Ballot[]
            {
                new Ballot{
                    Title = "FOR COMMANDER IN CREAM AND VICE ICE",
                    Subtitle = "(RANKED CHOICE VOTING (INSTANT RUNOFF))",
                    Description = "Rank candidates in order of choice. Mark your favorite candidate as first choice and then indicate your second and additional back-up choices in order of choice. You may rank as many candidates as you want.",
                    BallotType = Models.BallotType.Ranked},
                new Ballot{
                    Title = "FOR CHIEF DAIRY QUEEN",
                    Subtitle = "UNEXPIRED TERM (VOTE IN ONE OVAL))",
                    Description = "Shall Justice Mint C. Chip of the Supreme Court of the State of Ice Cream be retained in office for another term? Fill in the oval before the word \"YES\" if you wish the official to remain in office. Fill in the oval before the word \"NO\" if you do not wish the official to remain in office.",
                    BallotType = Models.BallotType.Unexpired},
                new Ballot{
                    Title = "FOR STATE REP. DISTRICT M&M",
                    Subtitle = "(VOTE FOR TWO)",
                    BallotType = Models.BallotType.Dual},
                new Ballot{
                    Title = "CONSTITUTIONAL INITIATIVE NO. 116",
                    Subtitle = "(VOTE IN ONE OVAL) MAKE VANILLA (OVER CHOCOLATE) THE OFFICAL BEST FLAVOR",
                    Description = "This is a fiercely debated topic and CI - 116 would official enumerate in written legislative text in perpetuity which flavor has favor - namely vanilla is better, unequivocally, than chocolate.",
                    BallotType = Models.BallotType.Unexpired},
                new Ballot{
                    Title = "President",
                    Subtitle = "(RANKED CHOICE VOTING (INSTANT RUNOFF))",
                    Description = "",
                    BallotType = Models.BallotType.Ranked},
                new Ballot{
                    Title = "State Rep.",
                    Subtitle = "(VOTE FOR TWO)",
                    Description = "",
                    BallotType = Models.BallotType.Dual},
                new Ballot{
                    Title = "Issue 2",
                    Subtitle = "(VOTE IN ONE OVAL)",
                    Description = "",
                    BallotType = Models.BallotType.Unexpired}
            };
            foreach (Ballot b in ballots)
            {
                context.Ballots.Add(b);
            }
            context.SaveChanges();

            var ballotOptions = new BallotOption[]
            {
                new BallotOption{Description="Reese WithoutASpoon - Democrat for C.I.C."},
                new BallotOption{Description="Cherry Garcia - Democrat for Vice Ice"},
                new BallotOption{Description="Choco 'Chip' Dough - Republican for C.I.C."},
                new BallotOption{Description="Carmela Coney - Republican for Vice Ice"},
                new BallotOption{Description="Magic Browny - Independent for C.I.C."},
                new BallotOption{Description="Phish Food - Independent for Vice Ice"},
                new BallotOption{Description="Yes"},
                new BallotOption{Description="No"},
                new BallotOption{Description="P. Nut Butter (REPUBLICAN)"},
                new BallotOption{Description="Cream C. Kol (INDEPENDENT)"},
                new BallotOption{Description="Marsh Mallow (DEMOCRAT)"},
                new BallotOption{Description="YES ON CI - 116 (FOR VANILLA)"},
                new BallotOption{Description="NO ON CI - 116 (NO VANILLA)"},
                new BallotOption{Description="Joe Dirt - Democrat"},
                new BallotOption{Description="Ricky Bobby - Republican"},
                new BallotOption{Description="The Tick - Independent"},
                new BallotOption{Description="George Lucas"},
                new BallotOption{Description="Guillermo Del Toro"},
                new BallotOption{Description="Peter Jackson"},
                new BallotOption{Description="YES ON Issue 2"},
                new BallotOption{Description="NO ON Issue 2"}
            };
            foreach (BallotOption o in ballotOptions)
            {
                context.BallotOptions.Add(o);
            }
            context.SaveChanges();

            var electionBallotAssignments = new ElectionBallotAssignment[]
            {
                new ElectionBallotAssignment
                {
                    BallotId = ballots.Single(c => c.Title == "FOR COMMANDER IN CREAM AND VICE ICE" ).Id,
                    ElectionId = elections.Single(i => i.Title == "FEDERAL AND STATE").Id
                },
                new ElectionBallotAssignment
                {
                    BallotId = ballots.Single(c => c.Title == "FOR CHIEF DAIRY QUEEN" ).Id,
                    ElectionId = elections.Single(i => i.Title == "FEDERAL AND STATE").Id
                },
                new ElectionBallotAssignment
                {
                    BallotId = ballots.Single(c => c.Title == "FOR STATE REP. DISTRICT M&M" ).Id,
                    ElectionId = elections.Single(i => i.Title == "FEDERAL AND STATE").Id
                },
                new ElectionBallotAssignment
                {
                    BallotId = ballots.Single(c => c.Title == "CONSTITUTIONAL INITIATIVE NO. 116" ).Id,
                    ElectionId = elections.Single(i => i.Title == "COUNTY").Id
                },
                new ElectionBallotAssignment
                {
                    BallotId = ballots.Single(c => c.Title == "CONSTITUTIONAL INITIATIVE NO. 116" ).Id,
                    ElectionId = elections.Single(i => i.Title == "Future Election").Id
                },
                new ElectionBallotAssignment
                {
                    BallotId = ballots.Single(c => c.Title == "President" ).Id,
                    ElectionId = elections.Single(i => i.Title == "Federal Election (Completed with Results)").Id
                },
                new ElectionBallotAssignment
                {
                    BallotId = ballots.Single(c => c.Title == "State Rep." ).Id,
                    ElectionId = elections.Single(i => i.Title == "Federal Election (Completed with Results)").Id
                },
                new ElectionBallotAssignment
                {
                    BallotId = ballots.Single(c => c.Title == "Issue 2" ).Id,
                    ElectionId = elections.Single(i => i.Title == "Federal Election (Completed with Results)").Id
                },
            };
            foreach (ElectionBallotAssignment electionBallotAssignment in electionBallotAssignments)
            {
                context.ElectionBallotAssignments.Add(electionBallotAssignment);
            }
            context.SaveChanges();

            var ballotOptionsAssignments = new BallotOptionAssignment[]
            {
                new BallotOptionAssignment
                {
                    BallotOptionId = ballotOptions.Single(c => c.Description == "Reese WithoutASpoon - Democrat for C.I.C." ).Id,
                    BallotId = ballots.Single(i => i.Title == "FOR COMMANDER IN CREAM AND VICE ICE").Id
                },
                new BallotOptionAssignment
                {
                    BallotOptionId = ballotOptions.Single(c => c.Description == "Choco 'Chip' Dough - Republican for C.I.C." ).Id,
                    BallotId = ballots.Single(i => i.Title == "FOR COMMANDER IN CREAM AND VICE ICE").Id
                },
                new BallotOptionAssignment
                {
                    BallotOptionId = ballotOptions.Single(c => c.Description == "Magic Browny - Independent for C.I.C." ).Id,
                    BallotId = ballots.Single(i => i.Title == "FOR COMMANDER IN CREAM AND VICE ICE").Id
                },
                new BallotOptionAssignment
                {
                    BallotOptionId = ballotOptions.Single(c => c.Description == "Yes" ).Id,
                    BallotId = ballots.Single(i => i.Title == "FOR CHIEF DAIRY QUEEN").Id
                },
                new BallotOptionAssignment
                {
                    BallotOptionId = ballotOptions.Single(c => c.Description == "No" ).Id,
                    BallotId = ballots.Single(i => i.Title == "FOR CHIEF DAIRY QUEEN").Id
                },
                new BallotOptionAssignment
                {
                    BallotOptionId = ballotOptions.Single(c => c.Description == "P. Nut Butter (REPUBLICAN)" ).Id,
                    BallotId = ballots.Single(i => i.Title == "FOR STATE REP. DISTRICT M&M").Id
                },
                new BallotOptionAssignment
                {
                    BallotOptionId = ballotOptions.Single(c => c.Description == "Cream C. Kol (INDEPENDENT)" ).Id,
                    BallotId = ballots.Single(i => i.Title == "FOR STATE REP. DISTRICT M&M").Id
                },
                new BallotOptionAssignment
                {
                    BallotOptionId = ballotOptions.Single(c => c.Description == "Marsh Mallow (DEMOCRAT)" ).Id,
                    BallotId = ballots.Single(i => i.Title == "FOR STATE REP. DISTRICT M&M").Id
                },
                new BallotOptionAssignment
                {
                    BallotOptionId = ballotOptions.Single(c => c.Description == "YES ON CI - 116 (FOR VANILLA)" ).Id,
                    BallotId = ballots.Single(i => i.Title == "CONSTITUTIONAL INITIATIVE NO. 116").Id
                },
                new BallotOptionAssignment
                {
                    BallotOptionId = ballotOptions.Single(c => c.Description == "NO ON CI - 116 (NO VANILLA)" ).Id,
                    BallotId = ballots.Single(i => i.Title == "CONSTITUTIONAL INITIATIVE NO. 116").Id
                },
                new BallotOptionAssignment
                {
                    BallotOptionId = ballotOptions.Single(c => c.Description == "Joe Dirt - Democrat" ).Id,
                    BallotId = ballots.Single(i => i.Title == "President").Id
                },
                new BallotOptionAssignment
                {
                    BallotOptionId = ballotOptions.Single(c => c.Description == "Ricky Bobby - Republican" ).Id,
                    BallotId = ballots.Single(i => i.Title == "President").Id
                },
                new BallotOptionAssignment
                {
                    BallotOptionId = ballotOptions.Single(c => c.Description == "The Tick - Independent" ).Id,
                    BallotId = ballots.Single(i => i.Title == "President").Id
                },
                new BallotOptionAssignment
                {
                    BallotOptionId = ballotOptions.Single(c => c.Description == "George Lucas" ).Id,
                    BallotId = ballots.Single(i => i.Title == "State Rep.").Id
                },
                new BallotOptionAssignment
                {
                    BallotOptionId = ballotOptions.Single(c => c.Description == "Guillermo Del Toro" ).Id,
                    BallotId = ballots.Single(i => i.Title == "State Rep.").Id
                },
                new BallotOptionAssignment
                {
                    BallotOptionId = ballotOptions.Single(c => c.Description == "Peter Jackson" ).Id,
                    BallotId = ballots.Single(i => i.Title == "State Rep.").Id
                },
                new BallotOptionAssignment
                {
                    BallotOptionId = ballotOptions.Single(c => c.Description == "YES ON Issue 2" ).Id,
                    BallotId = ballots.Single(i => i.Title == "Issue 2").Id
                },
                new BallotOptionAssignment
                {
                    BallotOptionId = ballotOptions.Single(c => c.Description == "NO ON Issue 2" ).Id,
                    BallotId = ballots.Single(i => i.Title == "Issue 2").Id
                }
            };
            foreach (BallotOptionAssignment ballotOptionAssignment in ballotOptionsAssignments)
            {
                context.BallotOptionAssignments.Add(ballotOptionAssignment);
            }
            context.SaveChanges();

            var votes = new Vote[]
            {
                new Vote{
                    ApplicationUserId = context.ApplicationUsers.Single(i => i.Email == "nathan@nathansmetana.com").Id,
                    ElectionId = elections.Single(i => i.Title == "Federal Election (Completed with Results)").Id,
                    BallotId = ballots.Single(i => i.Title == "President").Id,
                    BallotOptionId = ballotOptions.Single(i => i.Description == "Joe Dirt - Democrat").Id,
                    BallotOptionRank = 1
                },
                new Vote{
                    ApplicationUserId = context.ApplicationUsers.Single(i => i.Email == "nathan@nathansmetana.com").Id,
                    ElectionId = elections.Single(i => i.Title == "Federal Election (Completed with Results)").Id,
                    BallotId = ballots.Single(i => i.Title == "President").Id,
                    BallotOptionId = ballotOptions.Single(i => i.Description == "Ricky Bobby - Republican").Id,
                    BallotOptionRank = 2
                },
                new Vote{
                    ApplicationUserId = context.ApplicationUsers.Single(i => i.Email == "nathan@nathansmetana.com").Id,
                    ElectionId = elections.Single(i => i.Title == "Federal Election (Completed with Results)").Id,
                    BallotId = ballots.Single(i => i.Title == "President").Id,
                    BallotOptionId = ballotOptions.Single(i => i.Description == "The Tick - Independent").Id,
                    BallotOptionRank = 3
                },
                new Vote{
                    ApplicationUserId = context.ApplicationUsers.Single(i => i.Email == "nathan@nathansmetana.com").Id,
                    ElectionId = elections.Single(i => i.Title == "Federal Election (Completed with Results)").Id,
                    BallotId = ballots.Single(i => i.Title == "State Rep.").Id,
                    BallotOptionId = ballotOptions.Single(i => i.Description == "Guillermo Del Toro").Id
                },
                new Vote{
                    ApplicationUserId = context.ApplicationUsers.Single(i => i.Email == "nathan@nathansmetana.com").Id,
                    ElectionId = elections.Single(i => i.Title == "Federal Election (Completed with Results)").Id,
                    BallotId = ballots.Single(i => i.Title == "State Rep.").Id,
                    BallotOptionId = ballotOptions.Single(i => i.Description == "George Lucas").Id
                },
                new Vote{
                    ApplicationUserId = context.ApplicationUsers.Single(i => i.Email == "nathan@nathansmetana.com").Id,
                    ElectionId = elections.Single(i => i.Title == "Federal Election (Completed with Results)").Id,
                    BallotId = ballots.Single(i => i.Title == "Issue 2").Id,
                    BallotOptionId = ballotOptions.Single(i => i.Description == "YES ON Issue 2").Id
                },
                new Vote{
                    ApplicationUserId = context.ApplicationUsers.Single(i => i.Email == "fake@fake.com").Id,
                    ElectionId = elections.Single(i => i.Title == "Federal Election (Completed with Results)").Id,
                    BallotId = ballots.Single(i => i.Title == "President").Id,
                    BallotOptionId = ballotOptions.Single(i => i.Description == "Joe Dirt - Democrat").Id,
                    BallotOptionRank = 3
                },
                new Vote{
                    ApplicationUserId = context.ApplicationUsers.Single(i => i.Email == "fake@fake.com").Id,
                    ElectionId = elections.Single(i => i.Title == "Federal Election (Completed with Results)").Id,
                    BallotId = ballots.Single(i => i.Title == "President").Id,
                    BallotOptionId = ballotOptions.Single(i => i.Description == "Ricky Bobby - Republican").Id,
                    BallotOptionRank = 2
                },
                new Vote{
                    ApplicationUserId = context.ApplicationUsers.Single(i => i.Email == "fake@fake.com").Id,
                    ElectionId = elections.Single(i => i.Title == "Federal Election (Completed with Results)").Id,
                    BallotId = ballots.Single(i => i.Title == "President").Id,
                    BallotOptionId = ballotOptions.Single(i => i.Description == "The Tick - Independent").Id,
                    BallotOptionRank = 1
                },
                new Vote{
                    ApplicationUserId = context.ApplicationUsers.Single(i => i.Email == "fake@fake.com").Id,
                    ElectionId = elections.Single(i => i.Title == "Federal Election (Completed with Results)").Id,
                    BallotId = ballots.Single(i => i.Title == "State Rep.").Id,
                    BallotOptionId = ballotOptions.Single(i => i.Description == "Guillermo Del Toro").Id
                },
                new Vote{
                    ApplicationUserId = context.ApplicationUsers.Single(i => i.Email == "fake@fake.com").Id,
                    ElectionId = elections.Single(i => i.Title == "Federal Election (Completed with Results)").Id,
                    BallotId = ballots.Single(i => i.Title == "State Rep.").Id,
                    BallotOptionId = ballotOptions.Single(i => i.Description == "Peter Jackson").Id
                },
                new Vote{
                    ApplicationUserId = context.ApplicationUsers.Single(i => i.Email == "fake@fake.com").Id,
                    ElectionId = elections.Single(i => i.Title == "Federal Election (Completed with Results)").Id,
                    BallotId = ballots.Single(i => i.Title == "Issue 2").Id,
                    BallotOptionId = ballotOptions.Single(i => i.Description == "NO ON Issue 2").Id
                },

                new Vote{
                    ApplicationUserId = context.ApplicationUsers.Single(i => i.Email == "fake2@fake2.com").Id,
                    ElectionId = elections.Single(i => i.Title == "Federal Election (Completed with Results)").Id,
                    BallotId = ballots.Single(i => i.Title == "President").Id,
                    BallotOptionId = ballotOptions.Single(i => i.Description == "Joe Dirt - Democrat").Id,
                    BallotOptionRank = 2
                },
                new Vote{
                    ApplicationUserId = context.ApplicationUsers.Single(i => i.Email == "fake2@fake2.com").Id,
                    ElectionId = elections.Single(i => i.Title == "Federal Election (Completed with Results)").Id,
                    BallotId = ballots.Single(i => i.Title == "President").Id,
                    BallotOptionId = ballotOptions.Single(i => i.Description == "Ricky Bobby - Republican").Id,
                    BallotOptionRank = 1
                },
                new Vote{
                    ApplicationUserId = context.ApplicationUsers.Single(i => i.Email == "fake2@fake2.com").Id,
                    ElectionId = elections.Single(i => i.Title == "Federal Election (Completed with Results)").Id,
                    BallotId = ballots.Single(i => i.Title == "President").Id,
                    BallotOptionId = ballotOptions.Single(i => i.Description == "The Tick - Independent").Id,
                    BallotOptionRank = 3
                },
                new Vote{
                    ApplicationUserId = context.ApplicationUsers.Single(i => i.Email == "fake2@fake2.com").Id,
                    ElectionId = elections.Single(i => i.Title == "Federal Election (Completed with Results)").Id,
                    BallotId = ballots.Single(i => i.Title == "State Rep.").Id,
                    BallotOptionId = ballotOptions.Single(i => i.Description == "Guillermo Del Toro").Id
                },
                new Vote{
                    ApplicationUserId = context.ApplicationUsers.Single(i => i.Email == "fake2@fake2.com").Id,
                    ElectionId = elections.Single(i => i.Title == "Federal Election (Completed with Results)").Id,
                    BallotId = ballots.Single(i => i.Title == "State Rep.").Id,
                    BallotOptionId = ballotOptions.Single(i => i.Description == "Peter Jackson").Id
                },
                new Vote{
                    ApplicationUserId = context.ApplicationUsers.Single(i => i.Email == "fake2@fake2.com").Id,
                    ElectionId = elections.Single(i => i.Title == "Federal Election (Completed with Results)").Id,
                    BallotId = ballots.Single(i => i.Title == "Issue 2").Id,
                    BallotOptionId = ballotOptions.Single(i => i.Description == "NO ON Issue 2").Id
                }
            };
            foreach (Vote v in votes)
            {
                context.Votes.Add(v);
            }
            context.SaveChanges();
        }
    }
}